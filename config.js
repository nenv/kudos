'use strict';

exports.name = 'Kudos',
exports.port = process.env.PORT || 8080,
exports.mongodb = {
    uri: process.env.MONGOLAB_URI
};