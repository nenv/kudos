var gulp = require('gulp');
var gBump = require('gulp-bump');
var gSass = require('gulp-sass');

gulp.task('compile', function () {
  gulp.src('./public/scss/main.scss')
    .pipe(gSass())
    .pipe(gulp.dest('./public/scss/out'));
});

gulp.task('patch', function () {
  gulp.src('./package.json')
    .pipe(gBump())
    .pipe(gulp.dest('./'));
});

gulp.task('minor', function () {
  gulp.src('./package.json')
    .pipe(gBump({ type: 'minor' }))
    .pipe(gulp.dest('./'));
});

gulp.task('major', function () {
  gulp.src('./package.json')
  .pipe(gBump({ type: 'major' }))
  .pipe(gulp.dest('./'));
});