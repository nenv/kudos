'use strict';

require('newrelic');

//dependencies
var config = require('./config');
var express = require('express');
var mongoose = require('mongoose');
var winston = require('winston');
var http = require('http');
var path = require('path');

//create app
var app = express();

//bind config to app for persistent access
app.config = config;

//create app server
app.server = http.createServer(app);

//establish db connection
app.db = mongoose.createConnection(config.mongodb.uri);
app.db.on('error', function (error) { winston.log(config.name, 'mongoose connection error', error) });
app.db.once('open', function () {
  console.log('database is live!');
});

//server config
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//middleware
app.use(require('serve-static')(path.join(__dirname, 'public')));

//routes
require('./routes')(app);

//export app for testing
exports = module.exports = app;

//listen on app
app.server.listen(app.config.port, function () {
  console.log('server is live!');
});