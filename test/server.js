var app = require('../app');
var req = require('supertest');
var express = require('express');

describe('Server request', function () {
  it('should listen on /', function () { req(app).get('/').expect(200); });
});